package com.maxima.currency.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyPair
{

    @Enumerated(value = EnumType.STRING)
    private Pair pair;

    private Double value;

    @Override
    public String toString() {
        return pair + ":" + value;
    }
}
