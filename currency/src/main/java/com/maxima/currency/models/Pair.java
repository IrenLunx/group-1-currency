package com.maxima.currency.models;

public enum Pair
{
    EURRUB, EURUSD, USDRUB
}