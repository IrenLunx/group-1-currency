package com.maxima.currency.utility;

import com.maxima.currency.models.CurrencyPair;
import com.maxima.currency.models.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class CurrencyPairUtility
{

    public static List<CurrencyPair> currencyPairsHelper()
    {
        List<CurrencyPair> currencyPairs = new ArrayList<>();

        for (Pair pair : Pair.values())
        {
            double currencyValue = 0.0;

            switch (pair)
            {
                case EURRUB:
                case USDRUB:
                    currencyValue = ThreadLocalRandom.current().nextDouble(80.0, 120.0);
                    break;
                case EURUSD:
                    currencyValue = ThreadLocalRandom.current().nextDouble(1.0, 2.0);
                    break;
            }
            currencyPairs.add(CurrencyPair.builder().pair(pair).value(currencyValue).build());
        }
        return currencyPairs;
    }
}