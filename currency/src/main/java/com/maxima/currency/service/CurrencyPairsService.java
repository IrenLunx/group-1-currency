package com.maxima.currency.service;

import com.maxima.currency.utility.CurrencyPairUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class CurrencyPairsService
{
    private final KafkaProducerService kafkaProducerService;

    @Value("${spring.kafka.template.default-topic}")
    private String topic;

    @Autowired
    public CurrencyPairsService(KafkaProducerService kafkaProducerService)
    {
        this.kafkaProducerService = kafkaProducerService;
    }

    @Scheduled(cron = "${cron.value}")
    public void getCurrencyPairs()
    {
        kafkaProducerService.sendMessage(topic, CurrencyPairUtility.currencyPairsHelper().toString());
    }
}
